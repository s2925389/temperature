package nl.utwente.di.tempQuote;

import java.util.HashMap;

public class Quoter {
    public float getFahrenheit(String celsius) {
        int celsiusTemp = Integer.parseInt(celsius);
        return celsiusTemp * ((float) 9 /5) + 32;
    }
}
